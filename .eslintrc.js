const { eslint } = require('@coko/lint')

eslint.rules['jsx-a11y/alt-text'] = 0
eslint.rules['react/jsx-props-no-spreading'] = 0
eslint.rules['react/prop-types'] = 0

module.exports = eslint
