import React from 'react'
import styled from 'styled-components'

import { Alert, School, ThumbUp, Wand } from './icons'

const Wrapper = styled.div`
  background-color: #ededed;
  border-radius: 5px;
  display: flex;
  padding: 16px;

  svg {
    /* fill: rgb(51, 51, 51); */
    fill: dimgray;
  }
`

const IconArea = styled.div`
  padding-right: 16px;
  padding-top: 2px;
`

const Main = styled.div``

const MainHeader = styled.div`
  font-weight: bold;
`

const Content = styled.div``

const mapper = {
  alert: {
    header: 'Watch out',
    icon: <Alert />,
  },
  best: {
    header: 'Best practice',
    icon: <ThumbUp />,
  },
  info: {
    header: 'Info',
    icon: <School />,
  },
  tip: {
    header: 'Tip',
    icon: <Wand />,
  },
}

const Note = props => {
  const { children, type } = props

  return (
    <Wrapper>
      <IconArea>{mapper[type].icon}</IconArea>

      <Main>
        <MainHeader>{mapper[type].header}</MainHeader>
        <Content>{children}</Content>
      </Main>
    </Wrapper>
  )
}

export default Note
