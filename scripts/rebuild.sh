#!/bin/bash -x

# IN USE IN THE DEPLOYMENT SERVER.
# DO NOT EDIT ARBITRARILY. IT IS TRIGGERED BY THE CI PIPELINE.

export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

git pull

docker-compose -f docker-compose.production.yml down
docker-compose -f docker-compose.production.yml build
docker-compose -f docker-compose.production.yml up -d

exit
